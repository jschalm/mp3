package ca.ubc.ece.eece210.mp3.ast;

import static org.junit.Assert.*;
import ca.ubc.ece.eece210.mp3.Album;
import ca.ubc.ece.eece210.mp3.Catalogue;
import ca.ubc.ece.eece210.mp3.Element;
import ca.ubc.ece.eece210.mp3.Genre;
import ca.ubc.ece.eece210.mp3.ast.ASTNode;
import ca.ubc.ece.eece210.mp3.ast.QueryParser;
import ca.ubc.ece.eece210.mp3.ast.QueryTokenizer;
import ca.ubc.ece.eece210.mp3.ast.Token;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.junit.Test;

/**
 * This is a simple class that shows how the parser is supposed to work. Feel
 * free to play with it and see how ASTs are generated.
 * 
 * @author Sathish Gopalakrishnan
 * 
 */
public class QueryParserTest {
	
	// Global catalogue variables
	ArrayList<String> AAWSongList = new ArrayList<String>(Arrays.asList( "Intro",
		     "(Interlude 1)",
		     "Tesselate",
		     "Breezeblocks",
		     "(Interlude 2)",
		     "Something Good",
		     "Dissolve Me",
	      	 "Matilda",
	      	 "MS",
	      	 "Fitzpleasure",
	      	 "(Interlude 3)",
	      	 "Bloodflood",
	      	 "Taro" ));

	ArrayList<String> AMSongList = new ArrayList<String>(Arrays.asList(  "Do I Wanna Know",
			 "R U Mine?",
			 "One For The Road",
			 "Arabella",
			 "I Want It All",
			 "No. 1 Party Anthem",
			 "Mad Sounds",
			 "Fireside",
			 "Why'd You Only Call Me When You're High?",
			 "Snap Out Of It",
			 "Knee Socks",
			 "I Wanna Be Yours" ));

	ArrayList<String> SoSSongList = new ArrayList<String>(Arrays.asList( "Sultans of Swing",
			 "Lady Writer",
			 "Romeo and Juliet",
			 "Tunnel of Love",
			 "Private Investigations",
			 "Money For Nothing",
			 "Walk Of Life",
			 "Your Latest Trick",
			 "Local Hero",
			 "Skateaway",
			 "Telegraph Road" ));
	Catalogue catalogue;
	Genre Alt;
	Genre Rock;
	Genre classicRock;
	Album anAwesomeWave;
	Album AM;
	Album SultansOfSwing;

	@Test
	public void testIn() {
		List<Token> tokens = QueryTokenizer.tokenizeInput("in (\"Jazz\")");
		QueryParser parser = new QueryParser(tokens);
		ASTNode rootNode = parser.getRoot();
		assertEquals("in", rootNode.toString());
	}

	@Test
	public void testInAnd() {
		List<Token> tokens = QueryTokenizer
				.tokenizeInput("in (\"Jazz\") && matches (\"Ripley\")");
		QueryParser parser = new QueryParser(tokens);
		ASTNode root = parser.getRoot();
		assertEquals("(&& in matches)", root.toString());
	}

	/**
	 * This test should fail. After your complete the QueryParser
	 * implementation, this test should pass.
	 */
	@Test
	public void testOr() {
		List<Token> tokens = QueryTokenizer
				.tokenizeInput("in (\"Jazz\") || matches (\"Ripley\")");
		QueryParser parser = new QueryParser(tokens);
		ASTNode root = parser.getRoot();
		assertEquals("(|| in matches)", root.toString());
	}
	
	/**
	 * Tests the by() method
	 */
	@Test
	public void testByQuery() {
		initializeCatalogue();
		List<Element> searchResults = new ArrayList<Element>();
		
		searchResults = catalogue.query("by(\"Altj\")");
		assertTrue(searchResults.contains(anAwesomeWave));
		System.out.println(searchResults.toString());
	}
	
	/**
	 * Tests the in() method
	 */
	@Test
	public void testInQuery() {
		initializeCatalogue();
		List<Element> searchResults = new ArrayList<Element>();

		searchResults = catalogue.query("in(\"Rock\")");
		assertTrue(searchResults.contains(AM));
		assertTrue(searchResults.contains(SultansOfSwing));
		System.out.println(searchResults.toString());
	}
	
	/**
	 * Tests the matches() method
	 */
	@Test
	public void testMatchesQuery() {
		initializeCatalogue();
		List<Element> searchResults = new ArrayList<Element>();
		
		searchResults = catalogue.query("matches(\"An Awesome Wave\")");
		System.out.println(searchResults);
		assertTrue(searchResults.contains(anAwesomeWave));
	}
	
	/**
	 * Tests some more complicated queries
	 */
	@Test
	public void testComplicatedQueries() {
		initializeCatalogue();
		List<Element> searchResults = new ArrayList<Element>();
		
		searchResults = catalogue.query("in(\"Rock\") && by(\"Arctic Monkeys\") || matches(\"An Awesome Wave\")");
		assertTrue(searchResults.contains(AM));
		assertTrue(searchResults.contains(anAwesomeWave));
		
		searchResults = catalogue.query("by(\"Altj\") && in(\"Alt\")");
		System.out.println(searchResults);
		assertTrue(searchResults.contains(anAwesomeWave));
	}
	
	/**
	 * Tests an inNode individuallly
	 */
	@Test
	public void inNodeTest() {
	initializeCatalogue();
		
	Set<Element> albumsIn = new TreeSet<Element>();
	Token tokenIn = new Token(TokenType.IN,"in");
	InNode in = new InNode(tokenIn);
	in.setArguments("Rock");
	albumsIn = in.interpret(catalogue);
	System.out.println(albumsIn.toString());
	assertTrue(albumsIn.contains(AM));
	}
	
	/**
	 * Tests a  byNode individually
	 */
	@Test
	public void byNodeTest() {
	initializeCatalogue();
	
	Set<Element> albumsBy = new TreeSet<Element>();
	Token tokenBy = new Token(TokenType.BY,"by");
	ByNode by = new ByNode(tokenBy);
	by.setArguments("Arctic Monkeys");
	albumsBy = by.interpret(catalogue);
	System.out.println(albumsBy.toString());
	assertTrue(albumsBy.contains(AM));
	}
	
	/**
	 * Tests a matchesNode individually
	 */
	public void matchesNodeTest() {
		initializeCatalogue();
		
	Set<Element> albumMatches = new TreeSet<Element>();
	Token tokenMatches = new Token(TokenType.MATCHES,"matches");
	MatchesNode matches = new MatchesNode(tokenMatches);
	matches.setArguments("An Awesome Wave");
	albumMatches = matches.interpret(catalogue);
	System.out.println(albumMatches.toString());
	assertTrue(albumMatches.contains(anAwesomeWave));
	}
	
	/**
	 * Tests an andNode individually
	 */
	public void andNodeTest() {
		initializeCatalogue();

		
		Set<Element> And = new TreeSet<Element>();
		Token tokenAnd = new Token(TokenType.AND,"and");
		Token tokenIn = new Token(TokenType.IN,"in");
		Token tokenBy = new Token(TokenType.BY,"by");
		AndNode andNode = new AndNode(tokenAnd);
		InNode inNode = new InNode(tokenIn);
		ByNode byNode = new ByNode(tokenBy);
		inNode.setArguments("Alt");
		byNode.setArguments("Altj");
		andNode.addChild(byNode);
		andNode.addChild(inNode);
		And = andNode.interpret(catalogue);
		System.out.println(And.toString());
		assertTrue(And.contains(anAwesomeWave));
		assertFalse(And.contains(AM));
	}
	
	/**
	 * Tests and orNode individually
	 */
	@Test
	public void orNodeTest() {
		initializeCatalogue();
		
		Set<Element> Or = new TreeSet<Element>();
		Token tokenOR = new Token(TokenType.OR,"or");
		Token tokenIn = new Token(TokenType.IN,"in");
		Token tokenBy = new Token(TokenType.BY,"by");
		OrNode orNode = new OrNode(tokenOR);
		InNode inNode = new InNode(tokenIn);
		ByNode byNode = new ByNode(tokenBy);
		inNode.setArguments("Rock");
		byNode.setArguments("Altj");
		orNode.addChild(byNode);
		orNode.addChild(inNode);
		Or = orNode.interpret(catalogue);
		System.out.println(Or.toString());
		assertTrue(Or.contains(anAwesomeWave));
		assertTrue(Or.contains(AM));
	}
	
	 // Initialize a catalogue for testing
	public void initializeCatalogue() {
		Alt = new Genre("Alt");
		Rock = new Genre("Rock");
		classicRock = new Genre("classicRock");
		anAwesomeWave  = new Album( "An Awesome Wave", "Altj", AAWSongList);
		AM = new Album("AM", "Arctic Monkeys", AMSongList);
		SultansOfSwing = new Album("Sos", "DireStraits", SoSSongList);
		Alt.addToGenre(anAwesomeWave);
		Rock.addToGenre(AM);
		classicRock.addToGenre(SultansOfSwing);
		Rock.addToGenre(classicRock);
		catalogue = new Catalogue();
		catalogue.add(Alt);
		catalogue.add(Rock);
	}
}