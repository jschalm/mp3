package ca.ubc.ece.eece210.mp3.ast;

import java.util.Set;
import java.util.TreeSet;

import ca.ubc.ece.eece210.mp3.Album;
import ca.ubc.ece.eece210.mp3.Element;
import ca.ubc.ece.eece210.mp3.Catalogue;
import ca.ubc.ece.eece210.mp3.Genre;

public class InNode extends ASTNode {

	public InNode(Token token) {
		super(token);
	}

	@Override
	public Set<Element> interpret(Catalogue catalogue) {
		Set<Element> found = new TreeSet<Element>();
		for( Element e : catalogue.getContents() ) {
			if( e instanceof Genre ) {
				Genre g = (Genre) e;
				found.addAll(this.search(g));
			}
		}
		return found;
	}
	
	/**
	 * Finds and returns all albums in the given genre that are performed by the
	 * performer given by this node's argument string.
	 * @param g - The genre in which the method will search for albums.
	 * @return A set of all albums in the given genre that are performed by the given performer.
	 */
	private Set<Element> search(Genre g) {
		Set<Element> found = new TreeSet<Element>();
		if( g.getTitle().equals(this.arguments) ) {
			found.addAll(this.getAllAlbumsInGenre(g));
		}
		for( Element e : g.getChildren() ) {
			if( e instanceof Genre ) {
				Genre gg = (Genre) e;
				found.addAll(search(gg));
			}
		}
		return found;
	}
	
	/**
	 * Finds all albums that belong to a given genre, including albums in 
	 * its subgenres.
	 * @param g - the genre from which to find all albums
	 * @return A set of all albums contained by the given genre
	 */
	private Set<Element> getAllAlbumsInGenre(Genre g) {
		Set<Element> found = new TreeSet<Element>();
		for( Element e : g.getChildren() ) {
			if( e instanceof Album ) {
				Album a = (Album) e;
				found.add(a);
			}
			if( e instanceof Genre ) {
				Genre gg = (Genre) e;
				found.addAll(this.getAllAlbumsInGenre(gg));
			}
		}
		return found;
	}

}
