package ca.ubc.ece.eece210.mp3.ast;

import java.util.Set;

import ca.ubc.ece.eece210.mp3.Element;
import ca.ubc.ece.eece210.mp3.Catalogue;

/**
 * 
 * @author Sathish Gopalakrishnan
 * 
 */

public class AndNode extends ASTNode {

	/**
	 * Create a new AndNode given a parser token
	 * 
	 * @param token
	 */
	public AndNode(Token token) {
		super(token);
	}

	/**
	 * Interpret/evaluate an ANDNode of a query over a given catalogue
	 */
	@Override
	public Set<Element> interpret(Catalogue catalogue) {
		Set<Element> found = this.children.get(0).interpret(catalogue);
    	for( ASTNode child : this.children ) {
    		found.retainAll(child.interpret(catalogue));
    	}
    	return found;
	}

}
