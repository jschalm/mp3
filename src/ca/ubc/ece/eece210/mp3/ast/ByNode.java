package ca.ubc.ece.eece210.mp3.ast;

import java.util.Set;
import java.util.TreeSet;

import ca.ubc.ece.eece210.mp3.*;

public class ByNode extends ASTNode {
	
	public ByNode(Token token) {
		super(token);
	}
	
	/**
	 * Evaluates this leaf node for the given catalogue and finds and returns all albums
	 * that are performed by the performer given by this node's argument string.
	 * @param catalogue - The catalogue in which the method will search for albums.
	 * @return A set of all albums in the given catalogue that are performed by the given performer.
	 */
	@Override
	public Set<Element> interpret(Catalogue catalogue) {
		Set<Element> found = new TreeSet<Element>();
		for( Element e : catalogue.getContents() ) {
			if( e instanceof Genre ) {
				Genre g = (Genre) e;
				found.addAll(this.search(g));
			}
			if( e instanceof Album ) {
				Album a = (Album) e;
				if( this.albumIsBy(a) && !found.contains(a) ) {
					found.add(a);
				}
			}
		}
		return found;
	}
	
	/**
	 * Finds and returns all albums in the given genre that are performed by the
	 * performer given by this node's argument string.
	 * @param g - The genre in which the method will search for albums.
	 * @return A set of all albums in the given genre that are performed by the given performer.
	 */
	private Set<Element> search(Genre g) {
		Set<Element> found = new TreeSet<Element>();
		for( Element e : g.getChildren() ) {
			if( e instanceof Genre ) {
				Genre tempg = (Genre) e;
				found.addAll(search(tempg));
			}
			else if( e instanceof Album ) {
				Album tempa = (Album) e;
				if( albumIsBy(tempa) && !found.contains(tempa) ) {
					found.add(tempa);
				}
			}
		}
		return found;
	}
	
	/**
	 * Checks whether or not a given album is performed by the performer given by this node's argument string.
	 * @param a - The album to be checked
	 * @return - true if this album is performed by the given performer, false otherwise
	 */
	private boolean albumIsBy(Album a) {
		if( a.getPerformer().equals(this.arguments) ) {
			return true;
		}
		return false;
	}

}
