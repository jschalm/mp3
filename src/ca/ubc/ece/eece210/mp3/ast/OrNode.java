package ca.ubc.ece.eece210.mp3.ast;

import java.util.Set;
import java.util.TreeSet;

import ca.ubc.ece.eece210.mp3.Element;
import ca.ubc.ece.eece210.mp3.Catalogue;

public class OrNode extends ASTNode {

    public OrNode(Token token) {
	super(token);
    }

    @Override
    public Set<Element> interpret(Catalogue catalogue) {
    	Set<Element> found = new TreeSet<Element>();
    	for( ASTNode child : this.children ) {
    		for( Element e : child.interpret(catalogue)) {
    			if( !found.contains(e)) {
    				found.add(e);
    			}
    		}
    	}
    	return found;
    }

}
