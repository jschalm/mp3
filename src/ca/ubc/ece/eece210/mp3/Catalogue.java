package ca.ubc.ece.eece210.mp3;

import java.util.List;
import java.util.ArrayList;
import java.util.Set;

import ca.ubc.ece.eece210.mp3.ast.ASTNode;
import ca.ubc.ece.eece210.mp3.ast.QueryParser;
import ca.ubc.ece.eece210.mp3.ast.QueryTokenizer;
import ca.ubc.ece.eece210.mp3.ast.Token;

/**
 * Container class for all the albums and genres. Its main responsibility is to
 * save and restore the collection from a file.
 * 
 * @author Sathish Gopalakrishnan
 * 
 */
public final class Catalogue {

	private List<Element> contents;

	/**
	 * Builds a new, empty catalogue.
	 */
	public Catalogue() {
		contents = new ArrayList<Element>();
	}

	public int size() {
		return contents.size();
	}
	
	public Element get(int index) {
		return contents.get(index);
	}
	
	public void add(Element e) {
		contents.add(e);
	}

	/**
	 * Builds a new catalogue and restores its contents from the given file.
	 * 
	 * @param fileName
	 *            the file from where to restore the library.
	 */
	public Catalogue(String fileName) {
		// TODO implement
		// HINT:  look at Genre.restoreCollection(...)
	}

	/**
	 * Saved the contents of the catalogue to the given file.
	 * 
	 * @param fileName
	 *            the file where to save the library
	 */
	public void saveCatalogueToFile(String fileName) {
		// TODO implement
	}
	
	/**
	 * Returns the contents of this catalogue
	 * @return contents
	 */
	public List<Element> getContents() {
		return this.contents;
	}
	
	/**
	 * Parses and processes a search query over this catalogue.
	 * @param query - a string representing the query
	 * @return a list of albums found by the query
	 */
	public List<Element> query(String query) {
		List<Token> tokens = QueryTokenizer.tokenizeInput(query);
		QueryParser qp = new QueryParser(tokens);
		ASTNode root = qp.getRoot();
		Set<Element> set = root.interpret(this);
		List<Element> found = new ArrayList<Element>();
		for( Element e : set ) {
			found.add(e);
		}
		return found;
	}
}